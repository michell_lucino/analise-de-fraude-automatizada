
var auto_status_check = null; // Variável de controle para a Verificação do Status do Processamento //


function createToken() { // Cria um Token único para cada Análise //

  token = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
  
  return token;

}

function formValidation() { // Valida os Valores dados no Formulário //
  id_cliente = document.forms["analise-form"]["cliente_id"].value;
  cidade = document.forms["analise-form"]["cliente_cidade"].value;
  uf = document.forms["analise-form"]["cliente_uf"].value;
  contest = document.forms["analise-form"]["data_contest"].value;

  string_data = /^\d{4}\-\d{2}$/;
  string_re = /^[ãa-zA-ZáàâãéêíóôõúçÁÀÂÃÉÍÓÔÕÚ\s]*$/;

  if (isNaN(id_cliente)) { // Se o ID dado não for um Número //
    document.getElementById("alert-modal").innerHTML = "O campo \"ID do Cliente\" deve receber um número inteiro."; // Mensagem de Erro para o Alerta //
    document.getElementById("alert-modal").style.display = ""; // Mostra o Alerta com o Erro //

    return false;
  }
  if (string_re.test(cidade) == false) { // Cidade não tem apenas Letras //
    document.getElementById("alert-modal").innerHTML = "O campo \"Cidade\" deve receber apenas letras, sem nenhum caractere especial."; // Mensagem de Erro para o Alerta //
    document.getElementById("alert-modal").style.display = ""; // Mostra o Alerta com o Erro //

    return false;
  }
  if (string_re.test(uf) == false || uf.lenght > 2) { // UF não tem apenas Letras e/ou tem mais de 2 letras //
    document.getElementById("alert-modal").innerHTML = "O campo \"UF\" deve receber apenas 2 letras, sem nenhum caractere especial."; // Mensagem de Erro para o Alerta //
    document.getElementById("alert-modal").style.display = ""; // Mostra o Alerta com o Erro //

    return false;
  }
  if (string_data.test(contest) == false) { // Data Contestada não está no formato correto //
    document.getElementById("alert-modal").innerHTML = "O campo \"Ano-Mês Contestado\" deve receber uma data no formato YYYY-MM."; // Mensagem de Erro para o Alerta //
    document.getElementById("alert-modal").style.display = ""; // Mostra o Alerta com o Erro //

    return false;
  }

  return true;

}

function check_status() { // Checa o Status do Processamento Atual //
  $.ajax({
    url: '/status/'+ document.getElementById("analise-token").value,
    type: "GET",
    success: function(data) {

      status_info = data.split(";"); // Separa a response pelo ; //
      document.getElementById("mod_loading").innerHTML = status_info[0] + " - "+status_info[1]+"/"+status_info[2]; // Muda o Label com o Módulo Atual //
      document.getElementById("progress-bar").style.width = status_info[1] * (100 / status_info[2]) + "%"; // Aumenta o progresso da barra //

    },
    error: function (request, status, error) {
      console.log(request.responseText); // Printa o Erro no Console //
    }
  });
}

function download_analise(filename) { // Realiza o Download do Arquivo //

  window.open("/download-analise/" + filename, "_blank"); // Redireciona para a view de Download //

  document.getElementById("make_ana_btn").style.display = ""; // Mostra o botão de Realizar Análise //
  document.getElementById("download_btn").style.display = "none"; // Para de Mostrar o botão de Baixar Análise //

}

jQuery(document).ready(function(){

  $('#analise-form').submit(function(e){

    document.getElementById("analise-token").value = createToken(); // Gera token único para essa análise //

    is_form_valid = formValidation(); // Checa os valores dados pelo Usuário //

    if (is_form_valid) { // Se todos os valores forem válidos //

      var dados = jQuery(this).serialize();
      
      $("#analise_form :input").prop("disabled", true); // Desabilita todos os campos do formulário //
      document.getElementById("alert-modal").style.display = "none"; // Para de mostrar o Alerta com o Erro //
      document.getElementById("make_ana_btn").style.display = "none"; // Para de mostrar o botão de Realizar Análise //
      document.getElementById("loader").style.display = ""; // Mostra o loader //
      auto_status_check = setInterval(check_status, 30000); // Verificando Status a cada 30 seg //
      document.getElementById("cancel_btn").style.display = "";

      $.ajax({
          url: '/nova-analise/',
          type: "POST",
          data: dados,
          success: function(data) {

            document.getElementById("progress-bar").style.width = "100%"; // Faz a barra chegar ao 100% //
            $("#analise_form :input").prop("disabled", false); // Habilita todos os campos do formulário //

            if (data != "False") {

              document.getElementById("download_label").innerHTML = "Baixar "+ data; // Coloca o Label com o nome do Arquivo //
              document.getElementById("download_btn").style.display = ""; // Mostra o botão de Baixar Análise //

              $("#download_btn").attr("onclick", "download_analise(\""+data+"\");"); // Adiciona o evento com a função para o Download //

            }
            else {

              document.getElementById("alert-modal").innerHTML = "Erro ao estabelecer conexão o Banco! Verifique seus dados e tente novamente."; // Mensagem de Erro para o Alerta //
              document.getElementById("alert-modal").style.display = ""; // Mostra o Alerta com o Erro //
              document.getElementById("make_ana_btn").style.display = ""; // Mostra o botão de Realizar Análise //

            }

            document.getElementById("loader").style.display = "none"; // Para de mostrar o loader //
            clearInterval(auto_status_check); // Para a execução automática de checar o status do processamento //
            document.getElementById("mod_loading").innerHTML = ""; // Volta o status para apenas processando //
            document.getElementById("progress-bar").style.width = "0"; // Volta a barra a 0% //
            document.getElementById("cancel_btn").style.display = "none";
            
          },
          error: function (request, status, error) {

            console.log(request.responseText); // Printa o Erro no Console //
            
            document.getElementById("make_ana_btn").style.display = ""; // Mostra o botão de Realizar Análise //
            
            document.getElementById("loader").style.display = "none"; // Para de mostrar o loader //
            clearInterval(auto_status_check); // Para a execução automática de checar o status do processamento //
            document.getElementById("mod_loading").innerHTML = ""; // Volta o status para apenas processando //
            document.getElementById("progress-bar").style.width = "0"; // Volta a barra a 0% //
            document.getElementById("cancel_btn").style.display = "none";

          }
      });

    }
  
    e.preventDefault();

  });

  $('#download_btn').click(function(e){
    e.preventDefault();
  });

  $('#cancel_btn').click(function(e){
    e.preventDefault();
    window.open("/", "_self");
  });

});
