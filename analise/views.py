import os

from django.http import HttpResponse
from django.shortcuts import loader

from .functions.make_analise import makeAnalise


def index(request):
    template = loader.get_template('analise/index.html')
    context = {}
    
    return HttpResponse(template.render(context, request))


def new_analise(request):
    analise = makeAnalise(request.POST)
    
    return HttpResponse(analise)


def check_analise_status(request, token):
    status_file = open("analise/functions/status/"+str(token)+".txt", "r", encoding="utf-8")
    
    return HttpResponse(status_file.readline())


def download_analise(request, filename):
    path = "analise/functions/relatorios/"+filename
    
    if os.path.exists(path):
        with open(path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(path)
            
            return response


def error_404(request, exception=None):
    return HttpResponse('Error handler content', status=404)
