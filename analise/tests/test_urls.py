from django.test import SimpleTestCase
from django.urls import reverse, resolve
from analise.views import index, new_analise, check_analise_status, download_analise

class TestUrls(SimpleTestCase):

    def test_index_url_is_ok(self):
        url = reverse('index')
        self.assertEquals(resolve(url).func, index)
    

    def test_nova_analise_url_is_ok(self):
        url = reverse('new_analise')
        self.assertEquals(resolve(url).func, new_analise)

    
    def test_analise_status_url_is_ok(self):
        url = reverse('analise_status', args=['KAsdi4389ajisi9'])
        self.assertEquals(resolve(url).func, check_analise_status)
    

    def test_download_analise_url_is_ok(self):
        url = reverse('download_analise', args=['teste.xlsx'])
        self.assertEquals(resolve(url).func, download_analise)