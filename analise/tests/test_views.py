import sys
import os
import json

from django.test import TestCase, Client
from django.urls import reverse
from django.core.files import File


class TestViews(TestCase):

    def setUp(self):
        self.client = Client()

        self.index_url = reverse("index")
        self.new_analise_url = reverse("new_analise")

        f = open("analise/functions/status/zuklqqus8rdx7s6zg1ejsf.txt", "w+", encoding="utf-8")
        status_file = File(f)
        status_file.write("2;10;Historico de Logins")
        status_file.close()
        self.status_url = reverse("analise_status", args=["zuklqqus8rdx7s6zg1ejsf"])

        open("analise/functions/relatorios/Analise TESTE.xlsx", "w+", encoding="utf-8")
        self.download_url = reverse("download_analise", args=["Analise TESTE.xlsx"])


    def test_index_GET(self):
        response = self.client.get(self.index_url)
        
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "analise/index.html") 


    def test_new_analise_POST(self):
        response = self.client.post(self.new_analise_url, {
            "nome": "Gabriel Testes",
            "id": "4888",
            "cidade": "Rio de Janeiro",
            "uf": "RJ",
            "data_ini": "2019-11-11",
            "data_fin": "2019-11-11",
            "data_contest": "2019-11",
            "token": "zuklqqus8rdx7s6zg1ejsf"
        })

        self.assertEquals(response.status_code, 200)


    def test_analise_status_GET(self):
        response = self.client.get(self.status_url)

        self.assertEquals(response.status_code, 200)


    def test_download_analise_status_GET(self):
        response = self.client.get(self.download_url)

        os.remove("analise/functions/relatorios/Analise TESTE.xlsx")
        self.assertEquals(response.status_code, 200)

