import json
import sys
import os

from django.core.files import File

from analise.functions.lib import box_generator as bg
from analise.functions.lib import clean_relatorios as cl
from analise.functions.lib import view_format_box as vb
from analise.functions.lib import relatorio_config
from analise.functions.lib import utils as ut


def makeAnalise(analise_data):

    cl.clean_relatorios() # Limpa a pasta de Relatorios e Status, caso haja arquvios há mais de 2 dias

    usuario = os.environ.get('ACAUSERDB')
    
    password = os.environ.get('ACAPWDDB')
    

    if usuario == None or password == None:
        return False

    cursor = bg.get_cursor(usuario, password)

    if cursor == False: # Problema ao conectar ao Banco
        return False

    df_list = []

    analista = analise_data['nome']
    id_cliente = analise_data['id']
    cidade = analise_data['cidade']
    uf = analise_data['uf']
    dt_i = analise_data['data_ini']
    dt_f = analise_data['data_fin']
    dt_c= "{}-01".format(analise_data['data_contest'])

    etapa = 0

    for box in relatorio_config.config_box:
        f = open("analise/functions/status/"+str(analise_data['token'])+".txt" , "w+", encoding="utf-8")
        status_file = File(f)

        status_file.write(str(box[0]) + ";" + str(etapa) + ";" + str(len(relatorio_config.config_box)))
        status_file.close()
        
        querie = bg.le_query('./analise/functions/lib/queries/procedure', box[2], id_cliente, cidade, uf, dt_i, dt_f, dt_c)
        cursor.execute(querie)
        if box[1] != '':
            df_list.append({'titulo': box[0] ,'df':bg.get_box(cursor, "pvt" , box[1])})
            etapa += 1
        else:   
            df_list.append({'titulo': box[0] ,'df':bg.get_box1(cursor)})
            etapa += 1

    query = ut.getProdutosContratados(id_cliente)
    cursor.execute(query)
    tp_infos = ut.listToTupleInfo(cursor)

    sheet, book = vb.createXLSX()
    relIpBox = book.create_sheet("Relatorio detalhado de IPs")
    vb.informacoes_gerais(sheet, [analista, tp_infos[1], cidade, uf, ut.strMesAno(dt_i), ut.strMesAno(dt_f), ut.strMesAno(dt_c), tp_infos[0], tp_infos[2]])

    for dfa in df_list: 
        if(dfa['titulo'] == 'Consumos dos IPS'):
            #Cria a box de relatorio detalhado de IPs
            vb.makeBoxOnSheet(dfa['titulo'], dfa['df'], relIpBox, book)
        else:
            vb.makeBoxOnSheet(dfa['titulo'], dfa['df'], sheet, book)

    vb.makeDiagBox(sheet)
    vb.setStyles(sheet)
    vb.setBoxesBorder(sheet) 
    vb.setBoxesBorder(relIpBox) 

    book.save("analise/functions/relatorios/Analise {}.xlsx".format(tp_infos[1]))
    os.remove("analise/functions/status/"+str(analise_data['token'])+".txt")

    return "Analise {}.xlsx".format(tp_infos[1])
