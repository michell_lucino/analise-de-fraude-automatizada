import os
import sys
import time

from pathlib import Path


def clean_relatorios(): # Limpa todos os relatórios e status feitos há mais de 2 dias

    dias = 2 # Quantidade de dias desde a última modificação
    tempo_seg = time.time() - (dias * 24 * 60 * 60) # Passa os dias para Segundos e subtraí do Tempo atual
    rel_dir = Path('analise/functions/relatorios') # Caminho para a Pasta de Relatórios

    for rel_file in rel_dir.iterdir(): # Para cada Arquivo dentro da pasta
        file_path = 'analise/functions/relatorios/' + rel_file.name # Caminho para o arquivo
        file_data = os.stat(file_path) # Pegando informações do Arquivo para pegar última Data de Modificação

        if file_data.st_mtime <= tempo_seg: # Se a data de modificação foi há mais de 2 dias
            if os.path.exists(file_path): # Se o caminho para o Arquivo for Válido
                os.remove(file_path) # Remover o Relatório

    st_dir = Path('analise/functions/status') # Caminho para a Pasta de status

    for st_file in st_dir.iterdir(): # Para cada Arquivo dentro da pasta
        file_path = 'analise/functions/status/' + st_file.name # Caminho para o arquivo
        file_data = os.stat(file_path) # Pegando informações do Arquivo para pegar última Data de Modificação

        if file_data.st_mtime <= tempo_seg: # Se a data de modificação foi há mais de 2 dias
            if os.path.exists(file_path): # Se o caminho para o Arquivo for Válido
                os.remove(file_path) # Remover o Status
                