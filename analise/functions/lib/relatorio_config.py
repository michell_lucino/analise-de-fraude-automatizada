# Configuração de todas as Etapas e Procs usadas pelo Relatório
config_box = [
    ('Histórico de Logins', 'Quantidade de Consumo realizado', 'RL_HISTORICO_DE_LOGINS'),
    ('Histórico de Usuários', '', 'RL_USUARIO_INFO'),
    ('Histórico de IPs', 'IPs', 'RL_HISTORICO_DE_IPS'),
    ('Histórico de Provedores', 'Provedor', 'RL_HISTORICO_DE_PROVEDORES'),
    ('Histórico de Consultas', 'Quantidade de Consultas realizadas por horário', 'RL_HISTORICO_DE_HORARIOS'),
    ('Quantidade de Acessos Simultâneos (Crawlers)', 'Pico de consumo de acessos por min.', 'RL_MAXIMO_POR_MINUTO'),
    ('Consultas Realizadas por Produto', 'Produto', 'RL_HISTORICO_PRODUTO'),
    ('Histórico de Faturamento', 'Valores de acordo com mês consumido', 'RL_HISTORICO_FATURAMENTO'),
    ('Histórico de alteração de senha', '', 'RL_ALTERACAO_SENHA'),
    ('Consumos dos IPS', '', 'RL_DESCRICAO_IP')
]
