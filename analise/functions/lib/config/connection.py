import pymssql

def connect(usuario,senha):
    try:
        connection = pymssql.connect("dbdesenvolvimento-public.assertivasolucoes.com.br", usuario, senha,  "ALERTA")
        return connection.cursor()
    except:
        return False