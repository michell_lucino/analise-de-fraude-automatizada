from string import Template


def getProdutosContratados(id_cliente): # Retorna os Produtos que o Cliente Analisado possui
    filein = open( "{}.txt".format("analise/functions/lib/queries/produtos"))
    src = Template(filein.read()) 

    return src.substitute(id_cliente = id_cliente)


def listToTupleInfo(lista): # Transforma a Lista em Tupla
    list_str = ""
    cliente_login = ""
    contrato = ""

    for e in lista:
        list_str += e[0] + ", "   
        cliente_login = e[1]
        contrato = e[2]

    return (list_str[0:len(list_str) - 2], cliente_login, contrato)


def strMesAno(date): # Formata a Data dada para Ano/Mês por extenso
    mes = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", 
    "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]
    date = date.split("-")

    return str(date[0]) + "/" + mes[int(date[1]) - 1]
