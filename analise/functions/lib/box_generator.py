import pandas as pd
from string import Template

from analise.functions.lib.config import connection


month_dict = {
    'janeiro': 1,
    'fevereiro': 2,
    'março': 3,
    'abril': 4,
    'maio': 5,
    'junho': 6,
    'julho': 7,
    'agosto': 8,
    'setembro': 9,
    'outubro': 10,
    'novembro': 11,
    'dezembro': 12
}


def month_order(month):
    try:
        mes, ano = month.split('/')
        return [int(ano),month_dict[mes.lower()]]
    except:
        return [0,0]             



def get_cursor(usuario, senha):
    return connection.connect(usuario, senha)


def le_query(arquivo, relatorio ,id_cliente, cidade, estado, periodo_i, periodo_f, periodo_c):
    filein = open( "{}.txt".format(arquivo))
    src = Template(filein.read())  
    
    return src.substitute(proc = relatorio, id_cliente = id_cliente, \
        cidade = cidade, estado = estado, periodo_i=periodo_i, \
        periodo_f = periodo_f, periodo_c = periodo_c)


def get_box(cursor, column_name , description):
    column_names = [item[0] for item in cursor.description]
    
    df = pd.DataFrame(cursor.fetchall(), columns = column_names)
    
    df_p = df.pivot(index= column_name,  columns='MES').reset_index()
    
    df_p_n = df_p.droplevel([0], axis=1)
    df_p_n = df_p_n.rename(columns={"": description})
    
    columns = list(df_p_n.columns) 
    sorted(columns, key = month_order)
    
    df_p_n = df_p_n[sorted(list(df_p_n.columns), key = month_order)]
    
    return df_p_n.fillna(0)


def get_box1(cursor):
    column_names = [item[0] for item in cursor.description]
    df = pd.DataFrame(cursor.fetchall(), columns = column_names)
    
    return df.fillna(0)
