import os
import pandas as pd

from datetime import date
from openpyxl import Workbook
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl.styles import NamedStyle, Font, Border, Side, PatternFill, Alignment


# Todas as funções deste arquivo server para a Formatação do Excel #
boxes_info = []


def spacer(s): # Espaçamento de 2 Linhas
    s.append([])
    s.append([])


def createXLSX(): # Cria o novo arquivo Excel
    book = Workbook()
    sheet = book.active
    sheet.title = "Relatório de Análise de Fraude"

    return sheet, book


def setStyles(sheet): # Formata a planilha de Forma Geral
    b_white = Side(border_style="thin", color="FFFFFF")
    b_black = Side(border_style="medium", color="000000")

    max_row = sheet.max_row
    max_column = sheet.max_column

    # Fundo Branco
    for i in range(1, max_row + 10):
        for j in range(1, max_column + 10):
            sheet.cell(row = i, column = j).border = Border(top=b_white, left=b_white, right=b_white, bottom=b_white)

    # Borda Preta Tabela de Informações Gerais
    for i in range(1, 5):
        for j in range(1, 3):
            sheet.cell(row = i, column = 1).font = Font(size = 10, bold = True, name = "Arial")
            sheet.cell(row = i, column = 2).font = Font(size = 10, name = "Arial")
            sheet.cell(row = i, column = j).border = Border(top=b_black, left=b_black, right=b_black, bottom=b_black)

    # Styles das Informações do Relatório
    for i in range(10, 18):
        sheet.cell(row = i, column = 1).font = Font(size = 12, bold = True)
        sheet.cell(row = i, column = 2).font = Font(size = 12)


def setHeaderStyle(i, column, sheet): # Define o Estilo do Cabeçalho de cada Box
    # Style do Cabeçalho
    for j in range(1, column + 1):
        sheet.cell(row = i, column = j).fill = PatternFill("solid", fgColor="8ea9db")
        sheet.cell(row = i, column = j).alignment = Alignment(horizontal="center", vertical="center")
        sheet.cell(row = i, column = j).value = sheet.cell(row = i, column = j).value.title()


def setReaisOnValues(i, column, sheet): # Coloca o R$ para os valores monetários
    for j in range(1, column + 1):
        sheet.cell(row = i, column = j).value = "R$ " + sheet.cell(row = i, column = j).value

def boxCellAppendBorder(sheet,box):
    start_row = box[0]
    final_row = box[1]
    final_column = box[2]
    b_black = Side(border_style="thin", color="000000")
    # Borda Preta
    for i in range(start_row, final_row + 1):
        for j in range(1, final_column + 1):
            sheet.cell(row = i, column = j).border = Border(top=b_black, left=b_black, right=b_black, bottom=b_black)

    # Centralizando Texto
    for i in range(start_row + 1, final_row + 1):
        for j in range(2, final_column + 1):
            sheet.cell(row = i, column = j).alignment = Alignment(horizontal="center", vertical="center") 


def setBoxesBorder(sheet): # Coloca as bordas de todas as Boxes
    global boxes_info # Variável Global que guarda todas as Boxes
    #Verifica qual a sheet em que a informação será inserida
    if sheet.title == 'Relatorio detalhado de IPs':
        # Pega penultima box para o detalhado de IPs
        boxCellAppendBorder(sheet,boxes_info[-2])
    else:
        # Pega todas as box menos a penultima pois essa é de outra sheet.
        for box in boxes_info[:-2]:
            boxCellAppendBorder(sheet,box)
        boxCellAppendBorder(sheet,boxes_info[-1])
def informacoes_gerais(sheet, infos): # Coloca todas as informações gerais da Análise

    sheet.column_dimensions['A'].width = 50
    sheet.column_dimensions['B'].width = 15

    sheet.append(["Versão", "1.0"])
    sheet.append(["Data da análise", date.today().strftime("%d/%m/%Y")])
    sheet.append(["Analista de Suporte Responsável", infos[0]])
    sheet.append(["Nível de confidencialidade", "Restrito"])

    sheet.merge_cells('A5:B5')
    sheet.cell(row = 5, column = 1).font = Font(size = 11, bold = True)
    sheet.cell(row = 5, column = 1).alignment = Alignment(horizontal="center", vertical="center")
    sheet.cell(row = 5, column = 1).value = 'Assertiva, Segurança da Informação, Análise de consumo.'

    sheet.merge_cells('A7:B7') 
    sheet.cell(row = 7, column = 1).font = Font(size = 14, bold = True) 
    sheet.cell(row = 7, column = 1).alignment = Alignment(horizontal="center", vertical="center")
    sheet.cell(row = 7, column = 1).value = 'Relatório da Análise de Consumo'

    spacer(sheet)

    sheet.append(["Cliente login:", infos[1]])
    sheet.append(["Localização da Empresa:", infos[2] + " - " + infos[3]])
    sheet.append(["Início do contrado atual:", infos[8]])
    sheet.append(["Plano:"])
    sheet.append(["Período analisado:", infos[4] + " a " + infos[5]])
    sheet.append(["Período contestado:", infos[6]])
    sheet.append([])
    sheet.append(["Produtos Contratados:", infos[7]])

    spacer(sheet)


def makeBoxOnSheet(title, df, sheet, book): # Cria a Box desejada com as informações
    global boxes_info

    sheet.append([title])
    sheet.cell(row = sheet.max_row, column = 1).font = Font(size = 12, bold = True)

    header_row = sheet.max_row + 1

    rows = dataframe_to_rows(df, index=False)

    for row in rows:
        sheet.append(row)
        box_max_column = len(row)

    spacer(sheet)

    if title == "Histório de faturamento": # Colocando R$ nos Valores
        setReaisOnValues(header_row, box_max_column, sheet)

    setHeaderStyle(header_row, box_max_column, sheet) # Define o Estilo do Cabeçalho
    boxes_info.append([header_row, sheet.max_row, box_max_column]) # Adiciona informações da Box atual para a Lista de Boxes


def makeDiagBox(sheet): # Define a Box de Diagnóstico
    sheet.append(["Diagnóstico"])
    sheet.cell(row = sheet.max_row, column = 1).font = Font(size = 12, bold = True)

    sheet.merge_cells(start_row=sheet.max_row + 1, start_column=1, end_row=sheet.max_row + 1, end_column=5)
    sheet.cell(row = sheet.max_row, column = 1).alignment = Alignment(vertical="top")
    
    sheet.row_dimensions[sheet.max_row].height = 80

    boxes_info.append([sheet.max_row, sheet.max_row, 5])
