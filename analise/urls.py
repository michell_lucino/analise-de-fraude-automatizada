from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("analise/", views.index, name="index"),
    path("nova-analise/", views.new_analise, name="new_analise"),
    path("status/<str:token>", views.check_analise_status, name="analise_status"),
    path("download-analise/<str:filename>", views.download_analise, name="download_analise")
]